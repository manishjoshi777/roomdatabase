package com.mypplication.com.roomwithkotlin.activity

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.mypplication.com.roomwithkotlin.R
import com.mypplication.com.roomwithkotlin.adapter.DepartmentAdapter
import com.mypplication.com.roomwithkotlin.RoomHelper.RoomDatabaseClass
import com.mypplication.com.roomwithkotlin.listener.EmplyeeSelectedCallBack
import com.mypplication.com.roomwithkotlin.model.DepatmentTable
import com.mypplication.com.roomwithkotlin.util.DbWorkerThread
import com.mypplication.com.roomwithkotlin.viewModel.ViewModelClass
import kotlinx.android.synthetic.main.activity_add_department.*

class DepartmentActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var departmentList: MutableList<DepatmentTable>
    private lateinit var departmentDatabase: RoomDatabaseClass
    private lateinit var mDbWorkerThread: DbWorkerThread
    private val mUiHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_department)
        supportActionBar!!.hide()
        init()
        GetDepartmentList().execute()
    }

    @SuppressLint("StaticFieldLeak")
    inner class GetDepartmentList : AsyncTask<Void, Void, MutableList<DepatmentTable>>() {
        override fun doInBackground(vararg params: Void?): MutableList<DepatmentTable> {
            var departmentData : MutableList<DepatmentTable> = ArrayList<DepatmentTable>()
            try {
                departmentData.addAll(
                        departmentDatabase.getDataAccessDeo().getAllDepartment())
            }catch (e: Exception){
                e.printStackTrace()

            }

            return departmentData
        }

        override fun onPostExecute(result: MutableList<DepatmentTable>?) {
            if (result!!.size > 0){
                departmentList = result
                bindDataWithUi()
                Log.e("departmentId list", departmentList.toString())
            }else{
                Toast.makeText(this@DepartmentActivity,"No data in database", Toast.LENGTH_SHORT).show()
            }
            super.onPostExecute(result)
        }
    }

    private fun bindDataWithUi() {

        recyclerView.adapter = DepartmentAdapter(this@DepartmentActivity, departmentList, object : EmplyeeSelectedCallBack {
            override fun getDetalis(id: Int) {

            }

            override fun deleteDepartment(id: Int, position: Int) {

            }

        })
    }

    private fun init() {
        departmentList = ArrayList<DepatmentTable>()
        tvSubmit.setOnClickListener(this)
        imgFinish.setOnClickListener(this)
        departmentDatabase = RoomDatabaseClass.getInstance(this@DepartmentActivity)!!

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()

        recyclerView.layoutManager = LinearLayoutManager(this@DepartmentActivity)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.tvSubmit -> addDepartment()
            R.id.imgFinish -> finish()
        }
    }

    private fun addDepartment() {

        val dataViewModel = ViewModelProviders.of(this@DepartmentActivity).get(ViewModelClass::class.java)
        dataViewModel.addDepartment(this@DepartmentActivity, etDepartmentName).observe(this@DepartmentActivity, Observer {
            data ->
            Toast.makeText(this@DepartmentActivity, "departmentId Successfully added", Toast.LENGTH_SHORT).show()

        })


    }
}
