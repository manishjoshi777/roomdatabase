package com.mypplication.com.roomwithkotlin.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.PrimaryKey

/**
 * Created by manishjoshi on 21/8/18.
 */
@Entity(tableName = "employee_table",
        foreignKeys = arrayOf(ForeignKey(entity = DepatmentTable::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("departmentId"),
                onDelete = CASCADE)))
data class EmployeeTable(
        @ColumnInfo(name = "firstName") var firstName: String,
        @ColumnInfo(name = "lastName") var lastName: String,
        @ColumnInfo(name = "salary") var sal: Int,
        @ColumnInfo(name = "departmentId") var depId: Int)
{
    @PrimaryKey(autoGenerate = true)
    var id : Int = 0
}