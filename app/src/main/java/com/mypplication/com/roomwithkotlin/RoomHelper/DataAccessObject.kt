package com.mypplication.com.roomwithkotlin.RoomHelper

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.mypplication.com.roomwithkotlin.model.DepatmentTable
import com.mypplication.com.roomwithkotlin.model.EmployeeTable
import com.mypplication.com.roomwithkotlin.util.Constant
import com.mypplication.com.roomwithkotlin.util.Util

/**
 * Created by manishjoshi on 3/8/18.
 */
/**
 * DAO stands for DATA ACCESS OBJECT. This is basically an interface, which contains the methods
 * like getData() or storeData() etc.. which are used for accessing the database. This interface
 * will be implemented by the Room.
 */
@Dao
interface DataAccessObject {

    @Insert(onConflict = REPLACE)
    fun insert(deprecated: DepatmentTable)

    //list all department
    @Query("SELECT * from departments")
    fun getAllDepartment() : List<DepatmentTable>

    //list all employee
    @Query("SELECT * from employee_table")
    fun getAllEmployee() : List<EmployeeTable>

    @Insert(onConflict = REPLACE)
    fun insertImployee(employeeTable: EmployeeTable)

    //list Employee where name stand with A
    @Query("SELECT * from employee_table WHERE firstName LIKE 'A%'")
    fun getAllEmployeeStandWithA() : List<EmployeeTable>

    //get employee list whose salary stand with A
    @Query("SELECT * from employee_table WHERE departmentId =:id")
    fun getEmlpoyeeListOFSelecteddepartment(id : Int) :  List<EmployeeTable>

    //get employee list whose having second highest sal
    @Query("select * from employee_table where salary = (select max(salary) from employee_table where salary < (select max(salary)  from employee_table))")
    fun getSecondMaxSalary() : List<EmployeeTable>


    //get employee list whose Salary is grater then 10,000
    @Query("select * from employee_table where salary > 10000")
    fun getEmlpoyeeListSalaryGraterTenK() :  List<EmployeeTable>

    //get employee whose belong to selected department having highest salary
    @Query("select *, max(salary) from employee_table where departmentId =:id")
    fun getEmlpoyeeofHighestSalary(id : Int) :  List<EmployeeTable>

}