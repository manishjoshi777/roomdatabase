package com.mypplication.com.roomwithkotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mypplication.com.roomwithkotlin.R
import com.mypplication.com.roomwithkotlin.listener.EmplyeeSelectedCallBack
import com.mypplication.com.roomwithkotlin.model.EmployeeTable

/**
 * Created by manishjoshi on 21/8/18.
 */
class EmployeeAdapter(var context: Context, var employeeList: MutableList<EmployeeTable>,
                      var listener: EmplyeeSelectedCallBack) : RecyclerView.Adapter<EmployeeAdapter.MyViewHolder>() {
    var list= employeeList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.item_list, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txFirstName.text = list[position].firstName
        holder.txLastName.text = list[position].lastName
        holder.txId.text = list[position].depId.toString()
        holder.txSal.text= list[position].sal.toString()
    }


    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        var txFirstName : TextView = view.findViewById(R.id.tvFirstName)
        var txLastName : TextView = view.findViewById(R.id.tvLastName)
        var txId : TextView = view.findViewById(R.id.tvId)
        var txSal :TextView = view.findViewById(R.id.tvSal)
    }
}