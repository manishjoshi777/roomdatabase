package com.mypplication.com.roomwithkotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.mypplication.com.roomwithkotlin.R
import com.mypplication.com.roomwithkotlin.model.DepatmentTable

/**
 * Created by manishjoshi on 21/8/18.
 */
open class DepartmentSpinnerAdapter(context: Context, resource: Int, departmentList: MutableList<DepatmentTable>) :
        ArrayAdapter<DepatmentTable>(context, resource, departmentList) {

    var resource: Int
    var list: ArrayList<DepatmentTable>
    var inflater: LayoutInflater

    init {
        this.resource = resource
        this.list = departmentList as ArrayList<DepatmentTable>
        this.inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var retView: View?
        var name: TextView?
        retView = inflater.inflate(R.layout.spinner_list, parent, false);
        name = retView.findViewById(R.id.tvDepartment)

        name!!.text = list[position].depatment
        return retView
    }

}