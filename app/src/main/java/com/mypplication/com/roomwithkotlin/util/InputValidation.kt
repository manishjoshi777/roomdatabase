package com.mypplication.com.roomwithkotlin.util
import android.content.Context
import android.text.TextUtils
import android.widget.Toast

/**
 * Created by manishjoshi on 3/7/18.
 */
class InputValidation {

    fun ValidationCheck(context: Context, name:String="default", email:String ="default",
                        details : String = "default") : Boolean{

        when{

            name.isEmpty() ->{
                Toast.makeText(context, "Enter departmentId name", Toast.LENGTH_SHORT).show()
                return false
            }
            email.isEmpty() ->{
                Toast.makeText(context, "Enter email", Toast.LENGTH_SHORT).show()
                return false
            }

            details.isEmpty()->{
                Toast.makeText(context, "Enter departmentId details", Toast.LENGTH_SHORT).show()
                return false
            }

            email != "default" ->{
                if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    return true
                }else{
                    Toast.makeText(context, "Enter valid email", Toast.LENGTH_SHORT).show()
                    return false
                }
            }

        }

        return true
    }

}