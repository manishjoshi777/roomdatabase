package com.mypplication.com.roomwithkotlin.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.mypplication.com.roomwithkotlin.R
import com.mypplication.com.roomwithkotlin.adapter.DepartmentSpinnerAdapter
import com.mypplication.com.roomwithkotlin.model.DepartData
import com.mypplication.com.roomwithkotlin.model.DepatmentTable
import com.mypplication.com.roomwithkotlin.viewModel.ViewModelClass
import kotlinx.android.synthetic.main.activity_add_employee.*

class AddEmployeeActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    lateinit var departmentList : MutableList<DepatmentTable>
    var departmentId : Int = -1
    lateinit var list : List<DepartData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_employee)
        init()
        clickListener()
        getDepartmentList()
    }

    private fun clickListener() {
        spinnerDepartment.setOnItemSelectedListener(this)
        tvSubmit.setOnClickListener(this)
    }

    private fun getDepartmentList() {
        val dataViewModel = ViewModelProviders.of(this@AddEmployeeActivity).get(ViewModelClass::class.java)
        dataViewModel.getDepartmentList(this@AddEmployeeActivity).observe(this@AddEmployeeActivity, Observer {
            data ->
            if (data != null) {
                departmentList.addAll(data)
                Log.e("deparment List", departmentList.toString())
                setSpinnerAdapter()
            }else {
                Toast.makeText(this@AddEmployeeActivity, "something went wrong", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setSpinnerAdapter() {
        spinnerDepartment.adapter = DepartmentSpinnerAdapter(this@AddEmployeeActivity, R.layout.spinner_list, departmentList)

//        for (i in 0 until departmentList.size){
//
//        }

//        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, departmentList[0].depatment)
//        // Set layout to use when the list of choices appear
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        spinnerDepartment!!.adapter = aa
    }

    private fun init() {
        departmentList = ArrayList<DepatmentTable>()
        list = ArrayList<DepartData>()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        departmentId = departmentList[position].id
        Log.e("selected departmentId", departmentId.toString())
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.tvSubmit ->addEmployee()
        }
    }

    private fun addEmployee() {
        val dataViewModel = ViewModelProviders.of(this@AddEmployeeActivity).get(ViewModelClass::class.java)
        dataViewModel.addEmployee(this@AddEmployeeActivity, etFirstName, etLastName, etSalary, spinnerDepartment, departmentId).observe(this@AddEmployeeActivity, Observer {
            data ->
            if (data != null) {
                Toast.makeText(this@AddEmployeeActivity, "Employee Successfully added", Toast.LENGTH_SHORT).show()
            }else {
                Toast.makeText(this@AddEmployeeActivity, "something went wrong", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
