package com.mypplication.com.roomwithkotlin.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.mypplication.com.roomwithkotlin.R
import com.mypplication.com.roomwithkotlin.RoomHelper.RoomDatabaseClass
import com.mypplication.com.roomwithkotlin.adapter.EmployeeAdapter
import com.mypplication.com.roomwithkotlin.listener.EmplyeeSelectedCallBack
import com.mypplication.com.roomwithkotlin.model.EmployeeTable
import com.mypplication.com.roomwithkotlin.viewModel.ViewModelClass
import kotlinx.android.synthetic.main.activity_employee_list.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.doBeforeSdk

class EmployeeActivity : AppCompatActivity() {
    lateinit var employeeList : MutableList<EmployeeTable>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_list)
        supportActionBar!!.hide()
        init()

    }

    private fun init() {
        employeeList = ArrayList<EmployeeTable>()
        recyclerView.layoutManager = LinearLayoutManager(this@EmployeeActivity)
        var intentID = intent.getIntExtra("ID", -1)
        if (intentID == 1){
            getEmployeeList()
        }else if (intentID == 2){
            getEmployeeListStandWithA()
        }else if (intentID == 3){
            getEmployeeListWithSalaryGratertenK()
        }else if (intentID ==  4){
            getEmployeeListITDepartment()
        }else if (intentID == 5){
            getEmployeeSecondMaxSalary()
        }else if (intentID == 6){
            getEmployeeITDepartmentHeightSalary()
        }
    }

    private fun getEmployeeITDepartmentHeightSalary() {
        var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
        doAsync {
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@EmployeeActivity)!!.getDataAccessDeo().getEmlpoyeeofHighestSalary(3))
            }catch (e: Exception){
                e.printStackTrace()

            }
        }
        if (employeeData.size>0){
            if (employeeList.size >0){
                employeeList.clear()
            }
            employeeList.addAll(employeeData)
            callAdapter()
        }else{
            Toast.makeText(this@EmployeeActivity, "No data in database", Toast.LENGTH_SHORT).show()

        }
    }

    private fun getEmployeeSecondMaxSalary() {
        var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
        doAsync {
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@EmployeeActivity)!!.getDataAccessDeo().getSecondMaxSalary())
            }catch (e: Exception){
                e.printStackTrace()

            }
        }
        if (employeeData.size>0){
            if (employeeList.size >0){
                employeeList.clear()
            }
            employeeList.addAll(employeeData)
            callAdapter()
        }else{
            Toast.makeText(this@EmployeeActivity, "No data in database", Toast.LENGTH_SHORT).show()

        }

    }

    private fun getEmployeeListITDepartment() {
        var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
        doAsync {
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@EmployeeActivity)!!.getDataAccessDeo().getEmlpoyeeListOFSelecteddepartment(3))
            }catch (e: Exception){
                e.printStackTrace()

            }
        }
        if (employeeData.size>0){
            if (employeeList.size >0){
                employeeList.clear()
            }
            employeeList.addAll(employeeData)
            callAdapter()
        }else{
            Toast.makeText(this@EmployeeActivity, "No data in database", Toast.LENGTH_SHORT).show()

        }

    }

    private fun getEmployeeListWithSalaryGratertenK() {

        var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
        doAsync {
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@EmployeeActivity)!!.getDataAccessDeo().getEmlpoyeeListSalaryGraterTenK())
            }catch (e: Exception){
                e.printStackTrace()

            }
        }

        if (employeeData.size>0){
            if (employeeList.size >0){
                employeeList.clear()
            }
            employeeList.addAll(employeeData)
            callAdapter()
        }else{
            Toast.makeText(this@EmployeeActivity, "No data in database", Toast.LENGTH_SHORT).show()

        }

    }

    private fun getEmployeeListStandWithA() {
        var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
        doAsync {

            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@EmployeeActivity)!!.getDataAccessDeo().getAllEmployeeStandWithA())
            }catch (e: Exception){
                e.printStackTrace()

            }
        }

        if (employeeData.size>0){
            if (employeeList.size >0){
                employeeList.clear()
            }
            employeeList.addAll(employeeData)
            callAdapter()
        }else{
            Toast.makeText(this@EmployeeActivity, "No data in database", Toast.LENGTH_SHORT).show()

        }
    }

    private fun getEmployeeList() {
            val dataViewModel = ViewModelProviders.of(this@EmployeeActivity).get(ViewModelClass::class.java)
            dataViewModel.getEmployees(this@EmployeeActivity).observe(this@EmployeeActivity, Observer {
                data ->
                if (data != null) {
                    if (employeeList.size >0){
                        employeeList.clear()
                    }
                    employeeList.addAll(data)
                    callAdapter()
                }else {
                    Toast.makeText(this@EmployeeActivity, "departmentId Successfully added", Toast.LENGTH_SHORT).show()
                }
            })

    }

    private fun callAdapter() {

        recyclerView.adapter = EmployeeAdapter(this@EmployeeActivity, employeeList, object : EmplyeeSelectedCallBack{
            override fun getDetalis(positio: Int) {

            }

            override fun deleteDepartment(id: Int, positio: Int) {

            }

        })
    }
}
