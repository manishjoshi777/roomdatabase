package com.mypplication.com.roomwithkotlin.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by manishjoshi on 3/8/18.
 * Entity = A Java or a Kotlin class which represents a table within the database.
 */
@Entity(tableName = "departments")
data class DepatmentTable(@ColumnInfo(name = "departmentId") var depatment : String)
{
    @PrimaryKey(autoGenerate = true)
    var id : Int = 0
}