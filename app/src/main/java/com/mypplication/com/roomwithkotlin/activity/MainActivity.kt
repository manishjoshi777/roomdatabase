package com.mypplication.com.roomwithkotlin.activity

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.mypplication.com.roomwithkotlin.R
import com.mypplication.com.roomwithkotlin.RoomHelper.RoomDatabaseClass
import com.mypplication.com.roomwithkotlin.model.EmployeeTable
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        tvAddDepartment.setOnClickListener(this)
        tvEmployeeList.setOnClickListener(this)
        tvAddEmployee.setOnClickListener(this)
        tvEmployeeListStandWithA.setOnClickListener(this)
        tvEmployeeSalayGrater.setOnClickListener(this)
        tvEmployeeSecondMaxSalary.setOnClickListener(this)
        tvDetailsOfItDepartment.setOnClickListener(this)
        tvHighestSalaryOfDepartment.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when(v?.id){
            R.id.tvAddDepartment ->startActivity<DepartmentActivity>()
            R.id.tvEmployeeList -> startActivity<EmployeeActivity>("ID" to 1)
            R.id.tvAddEmployee -> startActivity<AddEmployeeActivity>()
            R.id.tvEmployeeListStandWithA -> startActivity<EmployeeActivity>("ID" to 2)
            R.id.tvEmployeeSalayGrater ->startActivity<EmployeeActivity>("ID" to 3)
            R.id.tvDetailsOfItDepartment -> startActivity<EmployeeActivity>("ID" to 4)
            R.id.tvEmployeeSecondMaxSalary ->startActivity<EmployeeActivity>("ID" to 5)
            R.id.tvHighestSalaryOfDepartment -> startActivity<EmployeeActivity>("ID" to 6)
        }

    }

    private fun getEmployeeListWithA(value: String) {
        if (value.equals("1")) {
            GetEmployeeListStandWithA().execute()
        }else if (value.equals("2")){
            GetEmployeeListWithSalaryGratertenK().execute()
        }else if (value.equals("3")){
            GetEmployeeListITDepartment().execute()
        }else if (value.equals("4")){
            GetEmployeeSecondMaxSalary().execute()
        }else{
            GetEmployeeITDepartmentHeightSalary().execute()
        }
    }
    @SuppressLint("StaticFieldLeak")
    inner class GetEmployeeITDepartmentHeightSalary : AsyncTask<Void, Void, MutableList<EmployeeTable>>() {
        override fun doInBackground(vararg params: Void?): MutableList<EmployeeTable> {
            var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@MainActivity)!!.getDataAccessDeo().getEmlpoyeeofHighestSalary(3))
            }catch (e: Exception){
                e.printStackTrace()

            }

            return employeeData
        }

        override fun onPostExecute(result: MutableList<EmployeeTable>?) {
            if (result!!.size > 0){
                Log.e("departmentId list", result.toString())
            }else{
                Toast.makeText(this@MainActivity,"No data in database", Toast.LENGTH_SHORT).show()
            }
            super.onPostExecute(result)
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class GetEmployeeListStandWithA : AsyncTask<Void, Void, MutableList<EmployeeTable>>() {
        override fun doInBackground(vararg params: Void?): MutableList<EmployeeTable> {
            var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@MainActivity)!!.getDataAccessDeo().getAllEmployeeStandWithA())
            }catch (e: Exception){
                e.printStackTrace()

            }

            return employeeData
        }

        override fun onPostExecute(result: MutableList<EmployeeTable>?) {
            if (result!!.size > 0){
                Log.e("departmentId list", result.toString())
            }else{
                Toast.makeText(this@MainActivity,"No data in database", Toast.LENGTH_SHORT).show()
            }
            super.onPostExecute(result)
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class GetEmployeeListWithSalaryGratertenK : AsyncTask<Void, Void, MutableList<EmployeeTable>>() {
        override fun doInBackground(vararg params: Void?): MutableList<EmployeeTable> {
            var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@MainActivity)!!.getDataAccessDeo().getEmlpoyeeListSalaryGraterTenK())
            }catch (e: Exception){
                e.printStackTrace()

            }

            return employeeData
        }

        override fun onPostExecute(result: MutableList<EmployeeTable>?) {
            if (result!!.size > 0){
                Log.e("departmentId list", result.toString())
            }else{
                Toast.makeText(this@MainActivity,"No data in database", Toast.LENGTH_SHORT).show()
            }
            super.onPostExecute(result)
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class GetEmployeeListITDepartment : AsyncTask<Void, Void, MutableList<EmployeeTable>>() {
        override fun doInBackground(vararg params: Void?): MutableList<EmployeeTable> {
            var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@MainActivity)!!.getDataAccessDeo().getEmlpoyeeListOFSelecteddepartment(3))
            }catch (e: Exception){
                e.printStackTrace()

            }

            return employeeData
        }

        override fun onPostExecute(result: MutableList<EmployeeTable>?) {
            if (result!!.size > 0){
                Log.e("department IT list", result.toString())
            }else{
                Toast.makeText(this@MainActivity,"No data in database", Toast.LENGTH_SHORT).show()
            }
            super.onPostExecute(result)
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class GetEmployeeSecondMaxSalary : AsyncTask<Void, Void, MutableList<EmployeeTable>>() {
        override fun doInBackground(vararg params: Void?): MutableList<EmployeeTable> {
            var employeeData : MutableList<EmployeeTable> = ArrayList <EmployeeTable>()
            try {
                employeeData.addAll(RoomDatabaseClass.getInstance(this@MainActivity)!!.getDataAccessDeo().getSecondMaxSalary())
            }catch (e: Exception){
                e.printStackTrace()

            }

            return employeeData
        }

        override fun onPostExecute(result: MutableList<EmployeeTable>?) {
            if (result!!.size > 0){
                Log.e("second highest", result.toString())
            }else{
                Toast.makeText(this@MainActivity,"No data in database", Toast.LENGTH_SHORT).show()
            }
            super.onPostExecute(result)
        }
    }


}
