package com.mypplication.com.roomwithkotlin.util

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import com.mypplication.com.roomwithkotlin.RoomHelper.RoomDatabaseClass
import com.mypplication.com.roomwithkotlin.model.DepatmentTable
import com.mypplication.com.roomwithkotlin.model.EmployeeTable
import org.jetbrains.anko.doAsync
import java.util.concurrent.Executors

/**
 * Created by manishjoshi on 21/8/18.
 */
class ViewModelUtil {

    fun addDepartment(context: Context, etDepartmentName: EditText): MutableLiveData<DepatmentTable> {
        val departments = MutableLiveData<DepatmentTable>()

        if (isValidationCheck(context, etDepartmentName)){
            var depatments=DepatmentTable(depatment = etDepartmentName.text.toString().trim())
            val service = Executors.newSingleThreadExecutor()
            service.submit(Runnable {
                try {
                    RoomDatabaseClass.getInstance(context)!!.getDataAccessDeo().insert(depatments)
                }catch (e :Exception){
                    e.printStackTrace()
                }

            })
            departments.value = depatments
//            //doAsync {
//            try {
//                RoomDatabaseClass.getInstance(context)!!.getDataAccessDeo().insert(depatments)
//                departments.value = depatments
//            }catch (e :Exception){
//                e.printStackTrace()
//            }
//            //}
        }

        return departments
    }

    private fun isValidationCheck(context: Context, etDepartmentName: EditText): Boolean {
        var inputValidation = InputValidation()

        when{
            !inputValidation.ValidationCheck(context, name = etDepartmentName.text.toString().trim()) ->return false
        }
        return true
    }

    fun getemployees(context: Context): MutableLiveData<List<EmployeeTable>> {

        val employees = MutableLiveData<List<EmployeeTable>>()
        var list : MutableList<EmployeeTable> = ArrayList<EmployeeTable>()
        doAsync {

            try {
                list.addAll(RoomDatabaseClass.getInstance(context)!!.getDataAccessDeo().getAllEmployee())
            }catch (e : Exception){
                Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show()
                Log.e("Exception", e.toString())
            }
        }
        employees.value = list
        return employees
    }

    fun getDepartMentList(context: Context): MutableLiveData<List<DepatmentTable>> {
        val departMentList = MutableLiveData<List<DepatmentTable>>()
        val list: MutableList<DepatmentTable> = ArrayList<DepatmentTable>()
        doAsync {
            try {
                list.addAll(RoomDatabaseClass.getInstance(context)!!.getDataAccessDeo().getAllDepartment())
            }catch (e :Exception){
               // Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show()
                Log.e("Exception", e.toString())
            }

        }

        departMentList.value = list

        return departMentList
    }

    fun addEmployee(context: Context, etFirstName: EditText, etLastName: EditText,
                    etSalary: EditText, spinner: Spinner, departmentId: Int): MutableLiveData<EmployeeTable> {

        val data = MutableLiveData<EmployeeTable>()

        if (Util(context).chekValidation(etFirstName, etLastName, etSalary, spinner)){
            var employeeData = EmployeeTable(firstName = etFirstName.text.toString().trim(),
                    lastName = etLastName.text.toString().trim(), sal = Integer.parseInt(etSalary.text.toString()),
                    depId = departmentId)
            doAsync {

                try {
                    RoomDatabaseClass.getInstance(context)!!.getDataAccessDeo().insertImployee(employeeData)

                }catch (e : Exception){
                    e.printStackTrace()
                }
            }
            data.value = employeeData
        }else{
            Toast.makeText(context, "check validation", Toast.LENGTH_SHORT).show()
        }

        return data
    }


}
