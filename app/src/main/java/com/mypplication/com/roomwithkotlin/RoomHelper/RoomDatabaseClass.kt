package com.mypplication.com.roomwithkotlin.RoomHelper

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.mypplication.com.roomwithkotlin.model.DepatmentTable
import com.mypplication.com.roomwithkotlin.model.EmployeeTable

/**
 * Created by manishjoshi on 3/8/18.
 * This is an abstract class that extends RoomDatabase, this is where you define the
 * entities (tables)and the version number of your database. It contains the database holder
 * and serves as the main access point for the underlying connection.
 */
@Database(entities = arrayOf(DepatmentTable::class, EmployeeTable::class), version = 1)
abstract class RoomDatabaseClass : RoomDatabase(){

    abstract fun getDataAccessDeo() : DataAccessObject

    companion object {
        private var instances : RoomDatabaseClass? =null

        fun getInstance(context: Context) : RoomDatabaseClass?{
            if (instances == null){
                instances = Room.databaseBuilder(context, RoomDatabaseClass::class.java, "sample.db").build()
            }
            return instances
        }

        fun destroyInstance(){
            instances = null
        }
    }
}