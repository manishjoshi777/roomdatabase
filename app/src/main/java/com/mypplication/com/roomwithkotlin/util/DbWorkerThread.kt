package com.mypplication.com.roomwithkotlin.util

import android.os.Handler
import android.os.HandlerThread

/**
 * Created by manishjoshi on 3/8/18.
 */
class DbWorkerThread(threadName: String) : HandlerThread(threadName){
    private lateinit var mWorkerHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        mWorkerHandler.post(task)
    }
}