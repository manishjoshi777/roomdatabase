package com.mypplication.com.roomwithkotlin.util

import android.content.Context
import android.widget.EditText
import android.widget.Spinner

/**
 * Created by manishjoshi on 22/8/18.
 */
class Util(var context: Context) {

    fun chekValidation(etFirstName: EditText, etLastName: EditText, etSalary: EditText, spinner: Spinner): Boolean {
        when{
            !InputValidation().ValidationCheck(context, name = etFirstName.text.toString().trim()) ->return false
            !InputValidation().ValidationCheck(context, name = etLastName.text.toString().trim()) ->return false
            !InputValidation().ValidationCheck(context, name = etSalary.text.toString().trim()) ->return false
            !InputValidation().ValidationCheck(context, name = spinner.selectedItem.toString().trim()) ->return false
        }

        return true
    }

    fun makePlaceholders(len: Int): String {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw RuntimeException("No placeholders")
        } else {
            val sb = StringBuilder(len * 2 - 1)
            sb.append("?")
            for (i in 1 until len) {
                sb.append(",?")
            }
            return sb.toString()
        }
    }
}