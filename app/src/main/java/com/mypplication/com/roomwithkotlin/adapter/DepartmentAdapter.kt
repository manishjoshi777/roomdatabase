package com.mypplication.com.roomwithkotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mypplication.com.roomwithkotlin.R
import com.mypplication.com.roomwithkotlin.listener.EmplyeeSelectedCallBack
import com.mypplication.com.roomwithkotlin.model.DepatmentTable

/**
 * Created by manishjoshi on 4/7/18.
 */
class DepartmentAdapter(var context: Context, val list: List<DepatmentTable>,
                        var listener : EmplyeeSelectedCallBack) : RecyclerView.Adapter<DepartmentAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.department_list, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //holder.txName.text = list[position].name
        holder.txDepartment.text = list[position].depatment
        Log.e("dataId", list[position].id.toString())
        //holder.txEmail.text = list[position].email
        holder.imgDelete.setOnClickListener { listener.deleteDepartment(list[position].id, position) }
        holder.itemView.setOnClickListener { listener.getDetalis(list[position].id) }
    }


    override
    fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

       // var txName : TextView = view.findViewById(R.id.txName)
        var txDepartment : TextView = view.findViewById(R.id.tvDepartment)
        var imgDelete : ImageView = view.findViewById(R.id.imgDelete)
        //var txEmail :TextView = view.findViewById(R.id.txEmail)

    }
}