package com.mypplication.com.roomwithkotlin.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.widget.EditText
import android.widget.Spinner
import com.mypplication.com.roomwithkotlin.model.DepatmentTable
import com.mypplication.com.roomwithkotlin.model.EmployeeTable
import com.mypplication.com.roomwithkotlin.util.ViewModelUtil
/**
 * Created by manishjoshi on 26/7/18.
 */
class ViewModelClass : ViewModel() {
    private val employeData = ViewModelUtil()

    fun addDepartment(context: Context, etDepartmentName: EditText): MutableLiveData<DepatmentTable> {
        return employeData.addDepartment(context, etDepartmentName)
    }

    fun getEmployees(context: Context) : MutableLiveData<List<EmployeeTable>>{
        return  employeData.getemployees(context)
    }

    fun getDepartmentList(context: Context) : MutableLiveData<List<DepatmentTable>>{
        return  employeData.getDepartMentList(context)
    }

    fun addEmployee(context: Context, etFirstName: EditText,
                    etLastName: EditText, etSalary: EditText, spinner: Spinner, departmentId: Int) : MutableLiveData<EmployeeTable>{
        return employeData.addEmployee(context, etFirstName, etLastName, etSalary, spinner, departmentId)
    }
}